package entities;

import lombok.Data;


@Data
public class Translate {
    String text;
    String translation;

}