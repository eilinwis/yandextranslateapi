package GateWay;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import entities.Translate;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GateWay {
    private static String URL = "https://translate.yandex.net/api/v1/tr.json";
    private static String Token = "";

    @SneakyThrows
    public Translate getTranslate(String text) {
        Gson gson = new Gson();
        HttpResponse<String> response = Unirest.get(URL)
                .header("X-Yandex-API-Key", Token)
                .queryString("text", text)
                .asString();
        String strResponse = response.getBody();
        log.info("response:"+ strResponse);
        return gson.fromJson(strResponse, Translate.class);
    }
}
