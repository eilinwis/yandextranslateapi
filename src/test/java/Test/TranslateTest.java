package Test;

import GateWay.GateWay;
import entities.Translate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


public class TranslateTest {
    private static final String translation = "Всем Привет!";

    @Test
    @DisplayName("Тест переводчика")
    void EngRusTranslation(){
        GateWay gateWay = new GateWay();
        Translate helloWorld = gateWay.getTranslate("Hello World!");
        Assertions.assertEquals(helloWorld.getTranslation(), translation);

    }

}
